const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// router for creating a course
router.post("/",auth.verify, (req, res) =>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(
		resultFromController))
})

// Route for retrieveing all the courses
router.get("/all", (req,res) =>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving specific course
router.get("/:courseId", (req,res) => {
 console.log(req.params.courseId);

 	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// route for updating certain course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => 	res.send(resultFromController));
})

// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	
});


module.exports = router;