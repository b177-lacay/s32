// imports the user model
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({ email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Controller function for user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// User authentication(/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
			if(result == null){
				return false
			}
			else{
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPasswordCorrect){
					// Generate an access token
					return {access : auth.createAccessToken(result)}
					}
				else {
						return false
					}
				}
	})
}

// Create a getProfile controller method for retrieving the details of the user:
//  - Find the document in the database using the user's ID
//  - Reassign the password of the returned document to an empty string
//  - Return the result back to the frontend

 module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Controller for enrolling the user to a specific course
module.exports.enroll = async (data) => {
	// Add the course Id in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseID in the user;s enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Save the updated user information in our database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true
			}
		})
	})
	// Add the user ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then( course => {
		// Adds the userID in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// SAve the updated course info
		return course.save().then((course,error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollment is successful 
	if(isUserUpdated && isCourseUpdated){
		return true
	}
	// User enrollemtns failed
	else{
		return false;
	}
}