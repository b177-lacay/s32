const Course = require("../models/Course");
const auth = require("../auth");

// Controller function for creating a course
module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};


// controller function for getting all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// controller function for retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
			return result;
	})
}

// Controller for updating course
module.exports.updateCourse = (reqParams, reqBody) =>{
	// specify the fields/properties of the documetns to be updated

	let updatedCourse = {
		name: reqBody.name,
		description : reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
				if(error){
					return false
				}
				else{
					return true
				}
	})
}

// Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
// [Activity] Controller function for archiving a course
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};


// Other option from Ms. jenny
/*const reqBody = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(reqBody.isAdmin == true) {
	courseController.addCourse(reqBody).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send(`Cannot add course. User is not an admin.`)
	}*/
