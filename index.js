const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors");
// Allows accesss to routes defined within the application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to our MongoDB
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.55eun.mongodb.net/S32-S36?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

// Prompts a message for succesfful database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

// Allows all resources to access the backend applications
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the /users string to be included for all user routes defined in the user route file
app.use("/users", userRoutes);

// Defines the course string to be included for all course routes in the course route file
app.use("/courses", courseRoutes);

// App listening to port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000 }`)
})